from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import auth
from django.contrib.auth import logout

# Create your views here.


def login(request):
    print(request.POST)
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            request.session['user'] = username
            response = HttpResponseRedirect('/testApi/home/')
            return response
        else:
            return render(request, 'testApi/login.html', {'error': '用户名或密码错误！'})
    return render(request, 'testApi/login.html')


def home(request):
    return render(request, 'testApi/home.html')


def logout(request):
    auth.logout(request)
    return render(request, 'testApi/login.html')
