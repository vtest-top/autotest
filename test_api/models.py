from django.db import models
from test_product import models as pro_models

# Create your models here.


class TestApi(models.Model):
    product_id = models.ForeignKey(pro_models.Product, on_delete=models.CASCADE)    # 项目ID
    api_name = models.CharField('接口名称', max_length=128)
    api_url = models.CharField('接口URL', max_length=128)
    api_body = models.CharField('请求Body', max_length=1024, null=True)
    request_method = (('GET', 'GET'), ('POST', 'POST'), ('PUT', 'PUT'), ('DELETE', 'DELETE'), ('PATCH', 'PATCH'),)
    api_method = models.CharField('请求方式', default='get', choices=request_method, max_length=8)
    api_response = models.CharField('响应报文', max_length=1024, null=True)
    create_time = models.DateTimeField('创建时间', auto_now=True)

    class Meta:
        verbose_name = '接口管理'
        verbose_name_plural = '接口管理'

    def __str__(self):
        return self.api_name


class TestCases(models.Model):
    testapi_id = models.ForeignKey(TestApi, on_delete=models.CASCADE)   # 接口ID
    testcase_name = models.CharField('用例名称', max_length=128)
    testcase_desc = models.CharField('用例描述', max_length=256, null=True)
    tester = models.CharField('执行人', max_length=64)
    result = models.BooleanField('测试结果')
    response = models.CharField('响应报文', max_length=1024, null=True)
    exp_result = models.CharField('期望结果', max_length=256, null=True)
    auth_result = models.CharField('实际结果', max_length=256, null=True)
    create_time = models.DateTimeField('创建时间', auto_now=True)

    class Meta:
        verbose_name = '用例管理'
        verbose_name_plural = '用例管理'

    def __str__(self):
        return self.testcase_name
