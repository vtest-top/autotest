from django.contrib import admin
from test_product import models

# Register your models here.


class ProductAdmin(admin.ModelAdmin):
    list_display = ['product_name', 'product_description', 'producter', 'create_time']


admin.site.register(models.Product, ProductAdmin)

