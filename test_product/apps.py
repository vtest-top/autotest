from django.apps import AppConfig


class TestProductConfig(AppConfig):
    name = 'test_product'
